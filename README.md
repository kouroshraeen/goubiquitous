# Go Ubiquitous

Go Ubiquitous is the sixth student project for the Udacity's [Android Developer Nanodegree by Google](https://www.udacity.com/course/android-developer-nanodegree-by-google--nd801).

## Screenshots
| Square Watch Face                     | Round Watch Face                    |
| ------------------------------------- |:-----------------------------------:|
|![screen](./art/square-watch-face.png) |![screen](./art/round-watch-face.png)|

## Project Description

In this project, I created a custom watch face for the [sunshine app](https://github.com/udacity/Advanced_Android_Development) to run on an Android Wear device.

## Project Specification

* App works on both round and square face watches.
* App displays the current time.
* App displays the high and low temperatures.
* App displays a graphic that summarizes the day’s weather (e.g., a sunny image, rainy image, cloudy image, etc.).
* App conforms to common standards found in the [Android Nanodegree General Project Guidelines](http://udacity.github.io/android-nanodegree-guidelines/core.html).
